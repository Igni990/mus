# README #

### In this repository: ###

* Java
* Maven
* Spring Boot
* Hibernate, JPA
* Thymeleaf
* Node JS

### How to run: ###

* Spring Boot:
* cd <home_dir>
* mvn spring-boot:run

* Node JS:
* cd <home_dir>\src\main\resources
* node site

