package ru.mus.utill;

import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;

@Component
public class RandomGenerator {
    public int genInt() {
        Random random = new Random( new Date().getTime() & 64);
        return random.nextInt();
    }
}
