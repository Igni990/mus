package ru.mus.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "ru.mus")
public class Configuration {

}
