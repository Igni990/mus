package ru.mus.rest;


import org.springframework.beans.factory.annotation.Autowired;
import ru.mus.view.console.ConsoleClient;
import ru.mus.view.console.ConsoleHelper;
import ru.mus.view.console.comands.Init;

public class AbstractController {

    @Autowired
    protected ConsoleClient consoleClient;

    @Autowired
    protected Init init;

    @Autowired
    protected ConsoleHelper consoleHelper;

}
