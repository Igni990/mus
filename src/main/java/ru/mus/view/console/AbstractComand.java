package ru.mus.view.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public abstract class AbstractComand<T> implements Comand<T> {

    @Autowired
    private Processor processor;

    protected abstract String key();

    @PostConstruct
    private void init() {
        processor.consume(key(), this);
    }
}
