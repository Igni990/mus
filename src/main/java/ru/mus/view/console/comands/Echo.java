package ru.mus.view.console.comands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mus.view.console.AbstractComand;
import ru.mus.view.console.ConsoleHelper;

@Component
public class Echo extends AbstractComand<Object> {

    @Autowired
    private ConsoleHelper console;

    protected String key() {
        return "echo";
    }

    @Override
    public Object execute(String mes) {
        console.print("Start echo:");
        String line;
        while (!"stop".equalsIgnoreCase(line = console.readLine())) {
            console.print(line);
        }
        console.print("Stop echo");
        return null;
    }


}
