package ru.mus.view.console.comands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.mus.utill.RandomGenerator;
import ru.mus.view.console.AbstractComand;


@Component
public class Init extends AbstractComand {

    @Autowired
    private RandomGenerator randomGenerator;

    @Override
    protected String key() {
        return "init";
    }

    @Override
    public Object execute(String mes) {
        return null;
    }
}
