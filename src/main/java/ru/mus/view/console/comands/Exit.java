package ru.mus.view.console.comands;

import org.springframework.stereotype.Component;
import ru.mus.view.console.AbstractComand;

@Component
public class Exit extends AbstractComand<Boolean> {

    private final String EXIT = "exit";

    @Override
    public Boolean execute(String mes) {
        return true;
    }

    @Override
    protected String key() {
        return EXIT;
    }
}
