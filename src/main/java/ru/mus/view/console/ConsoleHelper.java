package ru.mus.view.console;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class ConsoleHelper {

    private BufferedReader reader;

    @PostConstruct
    private void startReader() {
        this.reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public String readLine() {
        try {
            if (reader == null || !reader.ready()) {
                startReader();
            }
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void print(String mes) {
        System.out.println(mes);
    }
}
