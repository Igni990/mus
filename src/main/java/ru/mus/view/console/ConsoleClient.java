package ru.mus.view.console;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConsoleClient {

    @Autowired
    private Processor processor;

    @Autowired
    private ConsoleHelper console;

    public void start() {
        Thread daemon = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    while (true) {
                        processor.execute(console.readLine());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        daemon.setDaemon(true);
        daemon.start();
    }
}
