package ru.mus.view.console;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class Processor {

    private Map<String, Comand> comands = new HashMap<>();

    public Object execute(String mes) {
        return comands.containsKey(mes) ? comands.get(mes).execute(mes) : null;
    }

    public void consume(String mes, Comand comand) {
        comands.put(mes, comand);
    }
}
