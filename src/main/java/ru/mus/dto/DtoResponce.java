package ru.mus.dto;

import lombok.Getter;
import lombok.Setter;
import ru.mus.enums.Note;

public class DtoResponce {
    @Getter@Setter
    private Note first;
    @Getter@Setter
    private Note second;
    @Getter@Setter
    private Note third;
    @Getter@Setter
    private Note fourth;
    @Getter@Setter
    private Note fifth;
    @Getter@Setter
    private Note sixth;
    @Getter@Setter
    private Note seventh;
}
