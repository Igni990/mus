package ru.mus.enums;

public enum Note {

    C(0),
    C_IS(1),
    D(2),
    D_IS(3),
    E(4),
    F(5),
    F_IS(6),
    G(7),
    G_IS(8),
    A(9),
    A_IS(10),
    H(11);

    int pos;

    Note(int pos) {
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }

    public static Note findNoteForPos(int pos) {
        for (Note note : values()) {
            if (pos == note.getPos()) {
                return note;
            }
        }
        return null;
    }

    public static Note findNoteForOrdinal(int ord) {
        for (Note note : values()) {
            if (ord == note.ordinal()) {
                return note;
            }
        }
        return null;
    }
}
