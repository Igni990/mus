package ru.mus.enums;

public enum Mask {

    MAJOR(0,2,4,5,7,9,11),
    MINOR(0,2,3,5,7,8,10);

    public int[] mask;

    Mask(int...mask) {
        this.mask = mask;
    }

    public int[] getMask() {
        return mask;
    }
}
