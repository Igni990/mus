package ru.mus.service;

import javafx.scene.transform.Scale;
import org.springframework.stereotype.Service;
import ru.mus.dto.DtoRequest;
import ru.mus.dto.DtoResponce;
import ru.mus.enums.Mask;
import ru.mus.enums.Note;

@Service
public class ScaleService {

    public DtoResponce actualScale(DtoRequest request) {


        DtoResponce responce = new DtoResponce();
        return responce;
    }

    private Note[] selectNoteForMask(Mask mask, Note[] scale) {
        Note[] res = new Note[mask.getMask().length];
        int i = 0;
        for (int item : mask.getMask()) {
            res[i] = scale[item];
            i++;
        }

        return res;
    }

    private Note[] scaleStartWith(Note note) {
        Note[] res = new Note[12];
        int[] row = arr();
        int i = 0;
        for (int item : row) {
            item += note.ordinal();
            if (item > 11) {
                item -= 12;
            }
            res[i] = Note.findNoteForOrdinal(item);
            i++;
        }
        return res;
    }

    private int[] arr() {
        int i = 0;
        int[] row = new int[Note.values().length];
        for (Note item : Note.values()) {
            row[i] = item.ordinal();
            i++;
        }
        return row;
    }

    public static void main(String[] args) {
        ScaleService scaleService = new ScaleService();
        Note[] scale = scaleService.selectNoteForMask(Mask.MAJOR, scaleService.scaleStartWith(Note.C));
        for (Note note : scale) {
            System.out.println(note);
        }
    }
}
